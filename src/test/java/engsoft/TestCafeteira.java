package engsoft;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestCafeteira {
	private static Hardware h;
	private static Cafeteira c;
	
	@Before
	public void setUp() {
		h = new Hardware(false);
		c = new Cafeteira(h);
		h.iniciar();
	}
	
	@After
	public void tearDown() {
		c = null;
		h = null;
	}

	@Test
	public void testCafeteira() {
		testIHC();
	}
	
	private void testVaporizador() {
		
	}
	
	private void testAquecedor() {
		
	}
	
	private void testIHC() {
		// Espera
		assertEquals(EstadoIHC.naoFazendo, c.getEstadoIHC());

		h.ajustaNivelDeAgua(10);
		h.pressionaBotao();
		
		// Verificação Pronto
		assertEquals(10, h.pegaNivelDeAgua());
		assertEquals(0, h.pegaNivelDeCafe());
		assertEquals(EstadoHardware.ehEbulidorNaoVazio, h.leEstadoEbulidor());
		
		c.inicio();
		
		// Coando
		assertEquals(EstadoHardware.ehValvulaFechada, h.leEstadoValvulaPressao());
		assertEquals(EstadoHardware.ehEbulidorLigado, h.leEstadoElementoEbulidor());
		assertEquals(EstadoAquecedor.fazendoJarVazia, c.getEstadoAquecedor());
		assertEquals(EstadoVaporizador.vaporizando, c.getEstadoVaporizador());
		assertEquals(EstadoIHC.fazendo, c.getEstadoIHC());
		
		int i = h.pegaNivelDeAgua();
		while (i != 0) {
			h.executa();
			i--;
		}
		
		// Coação Completa
		assertEquals(0, h.pegaNivelDeAgua());
		assertEquals(10, h.pegaNivelDeCafe());
	}

}
